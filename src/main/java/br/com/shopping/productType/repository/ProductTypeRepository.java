package br.com.shopping.productType.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.shopping.productType.model.ProductType;

@Repository
@Transactional
public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {
  
}
