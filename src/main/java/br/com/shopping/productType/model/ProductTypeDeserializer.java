package br.com.shopping.productType.model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.shopping.productType.repository.ProductTypeRepository;

public class ProductTypeDeserializer extends JsonDeserializer<ProductType> {
  
  @Autowired
  ProductTypeRepository productTypeRepository;

  @Override
  public ProductType deserialize(JsonParser jsonParser, DeserializationContext arg1)
          throws IOException, JsonProcessingException {
    return productTypeRepository.findById(jsonParser.getLongValue()).get();
  }

}
