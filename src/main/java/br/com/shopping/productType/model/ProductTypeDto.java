package br.com.shopping.productType.model;

public class ProductTypeDto {
  
  private String productTypeName;

  public ProductType toModel() {
    return new ProductType(productTypeName);
  }

}
