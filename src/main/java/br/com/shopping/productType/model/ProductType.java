package br.com.shopping.productType.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.NoArgsConstructor;

@Entity
@Table(schema = "public", name = "product_type")
@NoArgsConstructor
public class ProductType {

  @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "product_type_name")
  private String productTypeName;

  public ProductType(String productTypeName) {
    this.productTypeName = productTypeName;
  }

  public Long getId() {
    return this.id;
  }

  public void setProductTypeName(String productTypeName) {
    this.productTypeName = productTypeName;
  }

  public String getProductTypeName() {
    return this.productTypeName;
  }

}
