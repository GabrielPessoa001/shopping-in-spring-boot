package br.com.shopping.productType.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.shopping.productType.model.ProductType;
import br.com.shopping.productType.service.ProductTypeService;

import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/api/v1/product/type")
public class ProductTypeController {
  
  @Autowired
  ProductTypeService productTypeService;

  @GetMapping
  public ResponseEntity<List<ProductType>> list() {
    return ResponseEntity.ok(productTypeService.list());
  }

  @GetMapping("{id}")
  public ResponseEntity<Optional<ProductType>> show(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID do tipo para busca") Long id) {
    return ResponseEntity.ok(productTypeService.show(id));
  }

  @PostMapping()
  public ResponseEntity<ProductType> create(@RequestBody ProductType productType) {
    return ResponseEntity.ok(productTypeService.create(productType));
  }

  @PutMapping
  public ResponseEntity<ProductType> update(@RequestBody ProductType productType) {
    return ResponseEntity.ok(productTypeService.update(productType));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID do tipo para exclusão") Long id) {  
    return productTypeService.delete(id);
  }

}
