package br.com.shopping.productType.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.shopping.productType.model.ProductType;
import br.com.shopping.productType.repository.ProductTypeRepository;

@Service
public class ProductTypeService {
  
  @Autowired
  ProductTypeRepository productTypeRepository;

  public List<ProductType> list() {
    return productTypeRepository.findAll();
  }

  public Optional<ProductType> show(Long id) {
    return productTypeRepository.findById(id);
  }

  public ProductType create(ProductType productType) {
    return productTypeRepository.save(productType);
  }

  public ProductType update(ProductType productType) {
    return productTypeRepository.save(productType);
  }

  public ResponseEntity<?> delete(Long id) {  
    if (productTypeRepository.existsById(id)) {
      productTypeRepository.deleteById(id);
      
      return ResponseEntity.status(HttpStatus.OK).body("Tipo do produto " + id + " apagado com sucesso.");
    }

    return ResponseEntity.notFound().build();
  }

}
