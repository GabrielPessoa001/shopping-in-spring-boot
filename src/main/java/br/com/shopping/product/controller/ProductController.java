package br.com.shopping.product.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import br.com.shopping.product.model.Product;
import br.com.shopping.product.model.ProductDto;
import br.com.shopping.product.service.ProductService;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
  
  @Autowired
  ProductService productService;

  @GetMapping
  public ResponseEntity<List<Product>> list() {
    return ResponseEntity.ok(productService.list());
  }

  @GetMapping("{id}")
  public ResponseEntity<Optional<Product>> show(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID do produto para busca") Long id) {
    return ResponseEntity.ok(productService.show(id));
  }

  @PostMapping()
  public ResponseEntity<Product> create(@Validated @RequestBody ProductDto dto) {
    return ResponseEntity.ok(productService.create(dto));
  }

  @PutMapping
  public ResponseEntity<Product> update(@Validated @RequestBody ProductDto dto) {
    return ResponseEntity.ok(productService.update(dto));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID do produto para exclusão") Long id) {  
    return productService.delete(id);
  }

}
