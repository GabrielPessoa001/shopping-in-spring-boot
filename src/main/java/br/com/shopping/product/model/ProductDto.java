package br.com.shopping.product.model;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import br.com.shopping.productType.model.ProductType;

public class ProductDto {
  
  @NotBlank(message = "O campo nome do produto não pode ficar em branco.")
  @Length(min = 3, max = 80, message = "O tamanho do campo deve conter mais de 3 letras e menos de 80.")
  private String productName;

  private String productPrice;

  private ProductType productType;

  public String getProductName() {
    return this.productName;
  }

  public Product toModel() {
    return new Product(productName, productPrice, productType);
  }

}
