package br.com.shopping.product.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.shopping.productType.model.ProductType;
import br.com.shopping.productType.model.ProductTypeDeserializer;
import lombok.NoArgsConstructor;

@Entity
@Table(schema = "public", name = "product")
@NoArgsConstructor
public class Product {

  @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "product_name")
  private String productName;

  @Column(name = "product_price")
  private String productPrice;

  @ManyToOne
  @JoinColumn(name = "product_type_id")
	@JsonDeserialize(using = ProductTypeDeserializer.class)
  private ProductType productType;

  public Product(String productName, String productPrice, ProductType productType) {
    this.productName = productName;
    this.productPrice = productPrice;
    this.productType = productType;
  }

  public Long getId() {
    return this.id;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductName() {
    return this.productName;
  }

  public void setProductPrice(String productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductPrice() {
    return this.productPrice;
  }

  public void setProductType(ProductType productType) {
    this.productType = productType;
  }

  public ProductType getProductType() {
    return this.productType;
  }

}
