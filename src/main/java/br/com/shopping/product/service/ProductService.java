package br.com.shopping.product.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import br.com.shopping.product.model.Product;
import br.com.shopping.product.model.ProductDto;
import br.com.shopping.product.repository.ProductRepository;

@Service
public class ProductService {
  
  @Autowired
  ProductRepository productRepository;

  public List<Product> list() {
    return productRepository.findAll();
  }

  public Optional<Product> show(Long id) {
    return productRepository.findById(id);
  }

  public Product create(ProductDto dto) {
    return productRepository.save(dto.toModel());
  }

  public Product update(ProductDto dto) {
    return productRepository.save(dto.toModel());
  }

  public ResponseEntity<?> delete(Long id) {  
    if (productRepository.existsById(id)) {
      productRepository.deleteById(id);
      
      return ResponseEntity.status(HttpStatus.OK).body("Produto " + id + " apagado com sucesso.");
    }

    return ResponseEntity.notFound().build();
  }

}
