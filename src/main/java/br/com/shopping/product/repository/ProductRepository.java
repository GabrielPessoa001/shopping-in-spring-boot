package br.com.shopping.product.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.shopping.product.model.Product;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Long> {
  
}
