package br.com.shopping.store.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;

import br.com.shopping.store.model.Store;
import br.com.shopping.store.service.StoreService;

@RestController
@RequestMapping("/api/v1/store")
public class StoreController {
  
  @Autowired
  StoreService storeService;

  @GetMapping
  public ResponseEntity<List<Store>> list() {
    return ResponseEntity.ok(storeService.list());
  }

  @GetMapping("{id}")
  public ResponseEntity<Optional<Store>> show(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID da loja para busca") Long id) {
    return ResponseEntity.ok(storeService.show(id));
  }

  @PostMapping()
  public ResponseEntity<Store> create(@RequestBody Store type) {
    return ResponseEntity.ok(storeService.create(type));
  }

  @PutMapping
  public ResponseEntity<Store> update(@RequestBody Store type) {
    return ResponseEntity.ok(storeService.update(type));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID da loja para exclusão") Long id) {  
    return storeService.delete(id);
  }

}
