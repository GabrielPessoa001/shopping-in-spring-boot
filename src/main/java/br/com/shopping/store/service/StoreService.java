package br.com.shopping.store.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.shopping.store.model.Store;
import br.com.shopping.store.repository.StoreRepository;

@Service
public class StoreService {
  
  @Autowired
  StoreRepository storeRepository;

  public List<Store> list() {
    return storeRepository.findAll();
  }

  public Optional<Store> show(Long id) {
    return storeRepository.findById(id);
  }

  public Store create(Store store) {
    return storeRepository.save(store);
  }

  public Store update(Store store) {
    return storeRepository.save(store);
  }

  public ResponseEntity<?> delete(Long id) {  
    if (storeRepository.existsById(id)) {
      storeRepository.deleteById(id);
      
      return ResponseEntity.status(HttpStatus.OK).body("Loja " + id + " apagado com sucesso.");
    }

    return ResponseEntity.notFound().build();
  }

}
