package br.com.shopping.store.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.shopping.store.model.Store;

@Repository
@Transactional
public interface StoreRepository extends JpaRepository<Store, Long> {
  
}
