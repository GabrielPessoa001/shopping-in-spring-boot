package br.com.shopping.store.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.shopping.storeType.model.StoreType;
import br.com.shopping.productType.model.ProductTypeDeserializer;
import lombok.NoArgsConstructor;

@Entity
@Table(schema = "public", name = "store")
@NoArgsConstructor
public class Store {

  @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "store_name")
  private String storeName;

  @JoinColumn(name = "store_type_id")
	@JsonDeserialize(using = ProductTypeDeserializer.class)
  @ManyToOne
  private StoreType storeType;

  public Store(String storeName, StoreType type) {
    this.storeName = storeName;
    this.storeType = type;
  }

  public Long getId() {
    return this.id;
  }

  public void setStoreName(String storeName) {
    this.storeName = storeName;
  }

  public String getStoreName() {
    return this.storeName;
  }

  public void setProductType(StoreType storeType) {
    this.storeType = storeType;
  }

  public StoreType getProductType() {
    return this.storeType;
  }

}
