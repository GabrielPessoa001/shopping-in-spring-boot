package br.com.shopping.store.model;

import br.com.shopping.storeType.model.StoreType;

public class StoreDto {
  
  private String storeName;
  private StoreType storeType;

  public Store toModel() {
    return new Store(storeName, storeType);
  }

}
