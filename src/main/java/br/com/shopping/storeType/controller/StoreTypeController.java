package br.com.shopping.storeType.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;

import br.com.shopping.storeType.model.StoreType;
import br.com.shopping.storeType.service.StoreTypeService;

@RestController
@RequestMapping("/api/v1/store/type")
public class StoreTypeController {
  
  @Autowired
  StoreTypeService storeTypeService;

  @GetMapping
  public ResponseEntity<List<StoreType>> list() {
    return ResponseEntity.ok(storeTypeService.list());
  }

  @GetMapping("{id}")
  public ResponseEntity<Optional<StoreType>> show(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID do tipo da loja para busca") Long id) {
    return ResponseEntity.ok(storeTypeService.show(id));
  }

  @PostMapping()
  public ResponseEntity<StoreType> create(@RequestBody StoreType storeType) {
    return ResponseEntity.ok(storeTypeService.create(storeType));
  }

  @PutMapping
  public ResponseEntity<StoreType> update(@RequestBody StoreType storeType) {
    return ResponseEntity.ok(storeTypeService.update(storeType));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable("id")
          @NotNull(message = "Você precisa informar o ID da loja para exclusão") Long id) {  
    return storeTypeService.delete(id);
  }

}
