package br.com.shopping.storeType.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.shopping.storeType.model.StoreType;
import br.com.shopping.storeType.repository.StoreTypeRepository;

@Service
public class StoreTypeService {
  
  @Autowired
  StoreTypeRepository storeTypeRepository;

  public List<StoreType> list() {
    return storeTypeRepository.findAll();
  }

  public Optional<StoreType> show(Long id) {
    return storeTypeRepository.findById(id);
  }

  public StoreType create(StoreType storeType) {
    return storeTypeRepository.save(storeType);
  }

  public StoreType update(StoreType storeType) {
    return storeTypeRepository.save(storeType);
  }

  public ResponseEntity<?> delete(Long id) {  
    if (storeTypeRepository.existsById(id)) {
      storeTypeRepository.deleteById(id);
      
      return ResponseEntity.status(HttpStatus.OK).body("Loja " + id + " apagado com sucesso.");
    }

    return ResponseEntity.notFound().build();
  }

}
