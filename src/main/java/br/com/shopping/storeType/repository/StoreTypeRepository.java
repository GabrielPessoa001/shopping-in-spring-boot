package br.com.shopping.storeType.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.shopping.storeType.model.StoreType;

@Repository
@Transactional
public interface StoreTypeRepository extends JpaRepository<StoreType, Long> {
  
}
