package br.com.shopping.storeType.model;

public class StoreTypeDto {
  
  private String storeName;

  public StoreType toModel() {
    return new StoreType(storeName);
  }

}
