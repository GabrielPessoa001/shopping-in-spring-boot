package br.com.shopping.storeType.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.NoArgsConstructor;

@Entity
@Table(schema = "public", name = "store_type")
@NoArgsConstructor
public class StoreType {

  @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "store_type_name")
  private String storeTypeName;

  public StoreType(String storeTypeName) {
    this.storeTypeName = storeTypeName;
  }

  public Long getId() {
    return this.id;
  }

  public void setStoreName(String storeTypeName) {
    this.storeTypeName = storeTypeName;
  }

  public String getStoreName() {
    return this.storeTypeName;
  }

}
